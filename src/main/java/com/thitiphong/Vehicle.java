package com.thitiphong;

public abstract class Vehicle {
    private String name ;
    private String engineName ;
    public Vehicle(String name,String engineName){
        this.name =name;
        this.engineName =engineName;
    }
    public String getName(){
        return name ;
    }
    public String getengineName() {
        return engineName ;
    }
    public void setName(String name){
        this.name =name ;
    }
    public void setnumberOfleg(String engineName){
        this.engineName = engineName;;
    }
   
}
