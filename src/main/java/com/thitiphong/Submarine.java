package com.thitiphong;

public class Submarine extends Vehicle implements Swimable {

    public Submarine(String name, String engineName) {
        super(name, engineName);

    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");

    }

    @Override
    public String toString() {
        return "Submarine(" + this.getName() + ")" + " enginename: " + this.getengineName();
    }
}
