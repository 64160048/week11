package com.thitiphong;

public class Bird extends Animal implements Flyable {

    public Bird(String name) {
        super(name, 2);
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " walk");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
        
    }
    @Override
    public String toString() {
        
        return "Bird(" +this.getName() + ")";
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
        
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");
        
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
        
    }
}
