package com.thitiphong;

public abstract  class Animal {
    private String name ;
    private int  numberOfLeg ;
    public Animal(String name,int numberOfLeg) {
        this.name =name ;
        this.numberOfLeg = numberOfLeg;
    }
    public String getName(){
        return name ;
    }
    public int getnumberOfleg() {
        return numberOfLeg ;
    }
    public void setName(String name){
        this.name =name ;
    }
    public void setnumberOfleg(int numberOfleg){
        this.numberOfLeg = numberOfleg;;
    }
    @Override
    public String toString() {
        return "Animal (" + name + ") has " + numberOfLeg + " legs";
    }
    public abstract void eat() ;
    public abstract void walk() ;
    public abstract void sleep() ;


}
