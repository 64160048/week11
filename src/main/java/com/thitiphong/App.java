package com.thitiphong;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.walk();
        bat1.sleep();
        bat1.fly();
        bat1.takeoff();
        bat1.landing();
        
        Fish fish1 = new Fish("Nemo");
        fish1.swim();
        fish1.eat();
        fish1.walk();
        fish1.sleep();
   
        Plane plane1 = new Plane("Boeing", "Boeing engine");
        plane1.fly();
        plane1.takeoff();
        plane1.landing();
   
        Bird bird1 = new Bird("Angrybird");
        bird1.eat();
        bird1.walk();
        bird1.sleep();
        bird1.fly();
        bird1.takeoff();
        bird1.landing();
   
        Submarine sub1 = new Submarine("Verginia", "Virginia engine");
        sub1.swim();

        Crocodile cro1 = new Crocodile("Bally");
        cro1.swim();
        cro1.crawl();
        cro1.eat();
        cro1.walk();
        cro1.sleep();

        Snake snake1 = new Snake("Anaconda");
        snake1.crawl();
        snake1.eat();
        snake1.walk();
        snake1.sleep();

        Rat rat1 = new Rat("Jerry");
        rat1.run();
        rat1.eat();
        rat1.walk();
        rat1.sleep();

        Dog dog1 = new Dog("Bus");
        dog1.run();
        dog1.eat();
        dog1.walk();
        dog1.sleep();

        Cat cat1 = new Cat("Tom");
        cat1.run();
        cat1.eat();
        cat1.walk();
        cat1.sleep();

        Human hu1 = new Human("Ple");
        hu1.crawl();
        hu1.swim();
        hu1.run();
        hu1.eat();
        hu1.walk();
        hu1.sleep();


        Flyable[] flyablesobject = {bat1,plane1,bird1};
        for(int i=0 ; i<flyablesobject.length ; i++) {
            flyablesobject[i].fly();
            flyablesobject[i].takeoff();
            flyablesobject[i].landing();
        }
        Swimable[] swimableobject = {fish1,sub1,cro1,hu1};
        for(int i=0 ; i<swimableobject.length ; i++){
            swimableobject[i].swim();
        }
        Crawlabe[] crawlableobject = {cro1,snake1,hu1};
        for(int i=0 ; i<crawlableobject.length ; i++){
            crawlableobject[i].crawl();
        }
        Runable[] runableobject = {rat1,cat1,dog1,hu1};
        for(int i=0 ; i<runableobject.length ; i++){
            runableobject[i].run();
        }
    
    
    
    
    
    
    }
}
