package com.thitiphong;

public class Crocodile extends Animal implements Crawlabe , Swimable {

    public Crocodile(String name) {
        super(name, 4);
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim");
        
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + " crawl");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " walk");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
        
    }
    @Override
    public String toString() {
        return "Crocodile(" +this.getName() + ")";
    } 
}
