package com.thitiphong;

public interface Flyable {
    public void fly();
    public void takeoff();
    public void landing();
}
