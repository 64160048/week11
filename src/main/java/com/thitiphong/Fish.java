package com.thitiphong;

public class Fish extends Animal implements Swimable {

    public Fish(String name) {
        super(name, 0);

    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");

    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");

    }
    @Override
    public String toString() {
        
        return "fish(" +this.getName() + ")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " can't walk");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim");
        
    }
}
